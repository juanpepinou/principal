package com.principal.crud;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "producto")
public class Producto {

    @DynamoDBHashKey
    private int id;

    @DynamoDBAttribute
    private String nombre;

    @DynamoDBAttribute
    private String descripcion;

    @DynamoDBAttribute
    private String marca;

    @DynamoDBAttribute
    private String modelo;

    @DynamoDBAttribute
    private String categoria;

    @DynamoDBAttribute
    private float descuento;
    @DynamoDBAttribute
    private float precio;

    public Producto() {
    }

    public Producto(int id, String nombre, String descripcion, String marca, String modelo, String categoria, float descuento, float precio) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.marca = marca;
        this.modelo = modelo;
        this.categoria = categoria;
        this.descuento = descuento;
        this.precio = precio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMarca() {
        return marca;
    }
    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCategoria() {
        return categoria;
    }
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public float getDescuento() {
        return descuento;
    }
    public void setDescuento(float descuento) {
        this.descuento = descuento;
    }

    public float getPrecio() {
        return precio;
    }
    public void setPrecio(float precio) {
        this.precio = ((100-this.descuento)/100)*precio;
    }
}
